package mmnet.lab;

import java.util.HashMap;
import java.util.Map;

import mmnet.lab.loglib.LogOpenHelper;
import mmnet.lab.loglib.Util;
import mmnet.lab.loglib.db.Entry;
import mmnet.lab.loglib.db.EntrySchema;
import mmnet.lab.loglib.db.UploadEntry;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class LogContentProvider extends ContentProvider {
	private static UriMatcher sUriMatcher;

	private static Map<Class<? extends UploadEntry>, String> mUrlEntryMap;
	private static Map<String, EntrySchema> mTypeIdEntrySchemaMap;

	private static String sAuthority;

	private static final int TYPE_COLLECTION = 0;
	private static final int TYPE_ITEM = 1;
	private static final int TYPE_NOT_UPLOADED_COLLECTION = 2;

	public static String getAuthority(Context c) {
		return c.getPackageName() + ".loglib";
	}

	public static Uri getBaseUri(Context c) {
		return new Uri.Builder().authority(getAuthority(c)).scheme("content").build();
	}

	public static EntrySchema getEntryClassByName(Context c, String entryClassId) {
		init(c);
		return mTypeIdEntrySchemaMap.get(entryClassId);
	}

	private static void init(Context c) {
		if (mUrlEntryMap == null) {
			mUrlEntryMap = Util.getUrlEntryMap(c);
			mTypeIdEntrySchemaMap = new HashMap<String, EntrySchema>();
			for (Class<? extends UploadEntry> entryClass : mUrlEntryMap.keySet()) {
				try {
					mTypeIdEntrySchemaMap.put(entryClass.getName(), entryClass.newInstance().getSchema());
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		if (sAuthority == null) {
			sAuthority = getAuthority(c);
		}
		if (sUriMatcher == null) {
			sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
			sUriMatcher.addURI(sAuthority, "*", TYPE_COLLECTION);
			sUriMatcher.addURI(sAuthority, "*/", TYPE_COLLECTION);
			sUriMatcher.addURI(sAuthority, "*/#", TYPE_ITEM);
			sUriMatcher.addURI(sAuthority, "*/#/", TYPE_ITEM);
		}
	}

	private LogOpenHelper mDatabaseHelper;

	@Override
	public String getType(Uri uri) {
		return null;
	}

	@Override
	public boolean onCreate() {
		Context cxt = getContext();
		init(getContext());
		mDatabaseHelper = new LogOpenHelper(cxt, mUrlEntryMap.keySet(), Util.getDatabaseVersion(cxt));
		return true;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Context cxt = getContext();
		synchronized (this) {
			init(cxt);
			switch (sUriMatcher.match(uri)) {
			case TYPE_COLLECTION: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
				long id = db.insert(schema.getTableName(), Entry.Columns.Id, values);
				db.close();
				return Uri.withAppendedPath(uri, Long.toString(id));
			}
			}
			throw new UnsupportedOperationException("Unsupported insertion.");
		}
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		Context cxt = getContext();
		synchronized (this) {
			init(cxt);
			switch (sUriMatcher.match(uri)) {
			case TYPE_COLLECTION: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
				Cursor c = db.query(schema.getTableName(), projection, selection, selectionArgs, null, null, sortOrder);
				return c;
			}
			case TYPE_ITEM: {
				throw new UnsupportedOperationException("Unimplemented query.");
			}
			case TYPE_NOT_UPLOADED_COLLECTION: {
				throw new UnsupportedOperationException("Unimplemented query.");
			}
			}
			throw new UnsupportedOperationException("Unsupported query.");
		}
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		Context cxt = getContext();
		synchronized (this) {
			init(cxt);
			switch (sUriMatcher.match(uri)) {
			case TYPE_ITEM: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				long id = Long.parseLong(uri.getPathSegments().get(1));
				SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
				int size = db.update(schema.getTableName(), values, String.format("%s = ?", Entry.Columns.Id), new String[] { Long.toString(id) });
				db.close();
				return size;
			}
			case TYPE_COLLECTION: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
				int size = db.update(schema.getTableName(), values, null, null);
				db.close();
				return size;
			}
			}
			throw new UnsupportedOperationException("Unsupported update:" + uri);
		}
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		Context cxt = getContext();
		synchronized (this) {
			init(cxt);
			switch (sUriMatcher.match(uri)) {
			case TYPE_COLLECTION: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
				Cursor c = db.query(schema.getTableName(), new String[0], selection, selectionArgs, null, null, null);
				int size = c.getCount();
				c.close();
				schema.dropTable(db);
				schema.createTable(db);
				db.close();
				return size;
			}
			case TYPE_ITEM: {
				EntrySchema schema = getEntryClassByName(cxt, uri.getPathSegments().get(0));
				int id = Integer.parseInt(uri.getPathSegments().get(1));
				SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
				int size = db.delete(schema.getTableName(), String.format("%s = ?", Entry.Columns.Id), new String[] { Long.toString(id) });
				db.close();
				return size;
			}
			}
			return 0;
		}
	}
}
