package mmnet.lab.loglib;

import java.util.Map;

import mmnet.lab.C;
import mmnet.lab.loglib.db.UploadEntry;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;

public class UploadService extends Service {

	private Map<Class<? extends UploadEntry>, String> mUrlEntryMap;
	private HandlerThread mUploadThread;
	private Handler mThreadHandler;

	@Override
	public void onCreate() {
		mUrlEntryMap = Util.getUrlEntryMap(this);
		mUploadThread = new HandlerThread("Upload");
		mUploadThread.start();
		mThreadHandler = new Handler(mUploadThread.getLooper());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		UploadEventReciver.setAlarm(this);

		Log.i(C.LOG_TAG, "Upload Triggered");
		if (Util.isLoggerLibEnabled(this)) {
			boolean celluat_upload = Util.isCelluarUploadEnabled(this);
			ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo info = manager.getActiveNetworkInfo();
			if (info != null && info.isConnected() && (info.getType() == ConnectivityManager.TYPE_WIFI || celluat_upload)) {
				mThreadHandler.post(new UploadRunnable(mUrlEntryMap, this));
			}
		}
		stopSelf();
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
}
