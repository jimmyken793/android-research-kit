package mmnet.lab.loglib.db;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import mmnet.lab.loglib.BeforeUploadException;
import mmnet.lab.loglib.OnUploadedException;
import mmnet.lab.loglib.Util;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public abstract class UploadEntry extends Entry {
	private static final String TAG = "UploadThread";

	@Column(value = "upload_time", requireUpload = false)
	public long uploadTime = 0;

	public void reset() {
		uploadTime = 0;
	}

	public void beforeUpload(Context c) throws BeforeUploadException {
	}

	public void onUploaded(Context c) throws OnUploadedException {
		markEntryFinish(c);
	}

	protected static List<UploadEntry> getUploadData(Context c, Class<? extends UploadEntry> entryClass) {
		List<UploadEntry> uploadingEntry = new ArrayList<UploadEntry>();
		EntrySchema schema = Util.getSchemaFromEntryClass(entryClass);
		if (schema == null) {
			return null;
		}
		Cursor cursor = schema.queryAll(c, UploadEntry.Columns.UPLOAD_TIME + "=?", new String[] { "0" });
		while (cursor.moveToNext()) {
			UploadEntry entry = schema.cursorToObject(cursor, Util.getEntryFromEntryClass(entryClass));
			uploadingEntry.add(entry);
		}
		if (cursor != null) {
			cursor.deactivate();
			cursor.close();
		}
		return uploadingEntry;
	}

	public static void uploadTable(Context c, Class<UploadEntry> entryClass, String url) throws ClientProtocolException, UnsupportedEncodingException, IOException {
		List<UploadEntry> uploadingEntry = getUploadData(c, entryClass);
		if (uploadingEntry.size() == 0) {
			return;
		}
		JSONArray dataArr = new JSONArray();
		final int batchSize = 10;
		int maxbatch = uploadingEntry.size() / batchSize + uploadingEntry.size() % batchSize == 0 ? 0 : 1;
		for (int batch = 0; batch < maxbatch; batch++) {
			List<UploadEntry> uploadedEntry = new ArrayList<UploadEntry>();
			for (int i = 0; i < batchSize; i++) {
				int index = batch * batchSize + i;
				if (index >= uploadingEntry.size()) {
					break;
				}
				UploadEntry entry = uploadingEntry.get(i);
				try {
					entry.getClass().getMethod("beforeUpload", Context.class).invoke(entry, c);
					dataArr.put(entry.getSchema().objectToJson(entry));
					uploadedEntry.add(entry);
				} catch (Exception e) {
					Log.e("LogLib", e.toString());
					e.printStackTrace();
				}
			}
			if (dataArr.length() == 0) {
				continue;
			}
			HttpResponse response = Util.postJSONData(dataArr.toString(), url);
			dataArr = null;
			String responseEntity = EntityUtils.toString(response.getEntity());
			try {
				Log.d(TAG, responseEntity);
				JSONObject json = (JSONObject) (new JSONTokener(responseEntity).nextValue());
				String status = json.getString("status");
				Log.d(TAG, "response.getStatusLine().getStatusCode():" + response.getStatusLine().getStatusCode());
				if (response.getStatusLine().getStatusCode() / 200 == 1) {
					Log.d(TAG, "Data successfully uploaded." + entryClass.getName());
					for (UploadEntry entry : uploadedEntry) {
						try {
							entry.getClass().getMethod("onUploaded", Context.class).invoke(entry, c);
						} catch (Exception e) {

						}
					}
					if (status.equals("ok")) {
						boolean resend = json.getJSONObject("message").getBoolean("resend");
						if (resend) {
							Log.w(TAG, "Data marked for resend.");
						}
					}
				} else {
					Log.e(TAG, "Server Response Error:" + response.getStatusLine().getStatusCode());
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	public void markEntryFinish(Context c) {
		uploadTime = new Date().getTime();
		getSchema().update(c, this);
	}
}