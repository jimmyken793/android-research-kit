package mmnet.lab.loglib.db;

import java.io.File;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mmnet.lab.LogContentProvider;
import mmnet.lab.loglib.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

public class EntrySchema {
	private static final String TAG = "Schema";

	private static final int TYPE_STRING = 0;
	private static final int TYPE_BOOLEAN = 1;
	private static final int TYPE_SHORT = 2;
	private static final int TYPE_INT = 3;
	private static final int TYPE_LONG = 4;
	private static final int TYPE_FLOAT = 5;
	private static final int TYPE_DOUBLE = 6;
	private static final int TYPE_BLOB = 7;
	private static final int TYPE_JSON_OBJECT = 8;
	private static final int TYPE_JSON_ARRAY = 9;
	private static final int TYPE_FILE = 10;

	private static final String SQLITE_TYPES[] = { "TEXT", "INTEGER", "INTEGER", "INTEGER", "INTEGER", "REAL", "REAL", "NONE", "TEXT", "TEXT", "TEXT" };

	private final String mTableName;
	private final Column[] mColumns;
	private final Map<Class<?>, List<String>> mColumnNamesByType;
	private final Class<? extends Entry> mEntryClass;

	private static boolean canBulkUpload = true;

	public Uri getUri(Context c) {
		return LogContentProvider.getBaseUri(c).buildUpon().appendPath(mEntryClass.getName()).build();
	}

	public static Uri getUri(Context c, Entry entry) {
		return LogContentProvider.getBaseUri(c).buildUpon().appendPath(entry.getClass().getName()).appendPath(Long.toString(entry.id)).build();
	}

	public List<String> getFieldsByType(Class<?> clazz) {
		List<String> list = mColumnNamesByType.get(clazz);
		if (list == null) {
			list = new ArrayList<String>();
			mColumnNamesByType.put(clazz, list);
			return list;
		}
		return list;
	}

	public EntrySchema(Class<? extends Entry> clazz) {
		mTableName = parseTableName(clazz);
		mColumns = parseColumn(clazz);
		mColumnNamesByType = parseColumnByType(clazz);
		mEntryClass = clazz;
	}

	public String parseTableName(Class<? extends Object> clazz) {
		Entry.Table table = clazz.getAnnotation(Entry.Table.class);
		Util.assertTrue(table != null);
		return table.value();
	}

	public String getTableName() {
		return mTableName;
	}

	private Map<Class<?>, List<String>> parseColumnByType(Class<? extends Object> clazz) {
		Map<Class<?>, List<String>> map = new HashMap<Class<?>, List<String>>();
		while (clazz != null) {
			parseColumnByType(clazz, map);
			clazz = clazz.getSuperclass();
		}
		return map;
	}

	private void parseColumnByType(Class<? extends Object> clazz, Map<Class<?>, List<String>> map) {
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			Entry.Column info = ((AnnotatedElement) field).getAnnotation(Entry.Column.class);
			if (info == null) {
				continue;
			}
			List<String> list = map.get(field.getType());
			if (list == null) {
				list = new ArrayList<String>();
			}
			list.add(field.getName());
		}
	}

	private Column[] parseColumn(Class<? extends Object> clazz) {
		ArrayList<Column> columns = new ArrayList<Column>();
		while (clazz != null) {
			parseColumn(clazz, columns);
			clazz = clazz.getSuperclass();
		}

		// Return a list.
		Column[] columnList = new Column[columns.size()];
		columns.toArray(columnList);
		return columnList;
	}

	private void parseColumn(Class<? extends Object> clazz, ArrayList<Column> columns) {
		Field[] fields = clazz.getDeclaredFields();
		for (Field field : fields) {
			Entry.Column info = ((AnnotatedElement) field).getAnnotation(Entry.Column.class);
			if (info == null) {
				continue;
			}
			int type;
			Class<?> fieldType = field.getType();
			if (fieldType == String.class) {
				type = TYPE_STRING;
			} else if (fieldType == boolean.class) {
				type = TYPE_BOOLEAN;
			} else if (fieldType == short.class) {
				type = TYPE_SHORT;
			} else if (fieldType == int.class) {
				type = TYPE_INT;
			} else if (fieldType == long.class) {
				type = TYPE_LONG;
			} else if (fieldType == float.class) {
				type = TYPE_FLOAT;
			} else if (fieldType == double.class) {
				type = TYPE_DOUBLE;
			} else if (fieldType == byte[].class) {
				type = TYPE_BLOB;
			} else if (fieldType == JSONObject.class) {
				type = TYPE_JSON_OBJECT;
			} else if (fieldType == JSONArray.class) {
				type = TYPE_JSON_ARRAY;
			} else if (fieldType == File.class) {
				type = TYPE_FILE;
				canBulkUpload = false;
			} else {
				throw new IllegalArgumentException("Unsupported field type for column: " + fieldType.getName());
			}
			Column newColumn = new Column(info.value(), type, field, info.defaultValue(), info.requireUpload());
			columns.add(newColumn);
		}
	}

	public boolean canBulkUpload() {
		return canBulkUpload;
	}

	public void createTable(SQLiteDatabase db) {
		StringBuilder sql = new StringBuilder("CREATE TABLE ");
		sql.append(mTableName);
		sql.append(" (" + Entry.Columns.Id + " INTEGER PRIMARY KEY AUTOINCREMENT");
		for (Column column : mColumns) {
			if (!column.isId()) {
				sql.append(',');
				sql.append(column.name);
				sql.append(' ');
				sql.append(SQLITE_TYPES[column.type]);
				if (!TextUtils.isEmpty(column.defaultValue)) {
					sql.append(" DEFAULT ");
					sql.append(column.defaultValue);
				}
			}
		}
		sql.append(");");
		db.execSQL(sql.toString());
	}

	public void dropTable(SQLiteDatabase db) {
		String tableName = mTableName;
		StringBuilder sql = new StringBuilder("DROP TABLE IF EXISTS ");
		sql.append(tableName);
		sql.append(';');
		db.execSQL(sql.toString());
		sql.setLength(0);
	}

	public Uri insert(Context c, Entry entry) {
		if (Util.isLoggerLibEnabled(c)) {
			Uri uri = getUri(c);
			Log.d("jizz", uri.toString());
			ContentValues values = new ContentValues();
			objectToValues(entry, values);
			if (entry.id == 0) {
				values.remove(Entry.Columns.Id);
			}
			return c.getContentResolver().insert(uri, values);
		} else {
			return null;
		}
	}

	public int update(Context c, Entry entry) {
		Uri uri = getUri(c, entry);
		ContentValues values = new ContentValues();
		entry.getSchema().objectToValues(entry, values);
		values.remove(Entry.Columns.Id);
		return c.getContentResolver().update(uri, values, null, null);
	}

	public int delete(Context c, long id) {
		return 0;
	}

	public void objectToValues(Entry object, ContentValues values) {
		try {
			for (Column column : mColumns) {
				String columnName = column.name;
				Field field = column.field;
				switch (column.type) {
				case TYPE_STRING:
					values.put(columnName, (String) field.get(object));
					break;
				case TYPE_BOOLEAN:
					values.put(columnName, field.getBoolean(object));
					break;
				case TYPE_SHORT:
					values.put(columnName, field.getShort(object));
					break;
				case TYPE_INT:
					values.put(columnName, field.getInt(object));
					break;
				case TYPE_LONG:
					values.put(columnName, field.getLong(object));
					break;
				case TYPE_FLOAT:
					values.put(columnName, field.getFloat(object));
					break;
				case TYPE_DOUBLE:
					values.put(columnName, field.getDouble(object));
					break;
				case TYPE_BLOB:
					values.put(columnName, (byte[]) field.get(object));
					break;
				case TYPE_JSON_OBJECT:
					if (field.get(object) != null) {
						values.put(columnName, ((JSONObject) field.get(object)).toString());
					} else {
						values.put(columnName, new JSONObject().toString());
					}
					break;
				case TYPE_JSON_ARRAY:
					if (field.get(object) != null) {
						values.put(columnName, ((JSONArray) field.get(object)).toString());
					} else {
						values.put(columnName, new JSONArray().toString());
					}
					break;
				case TYPE_FILE:
					if (field.get(object) != null) {
						values.put(columnName, ((File) field.get(object)).getAbsolutePath());
					}
				}
			}
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public JSONObject objectToJson(Entry object) {
		JSONObject json = new JSONObject();
		try {
			for (Column column : mColumns) {
				if (!column.requireUpload) {
					continue;
				}
				String columnName = column.name;
				Field field = column.field;
				switch (column.type) {
				case TYPE_STRING:
					json.put(columnName, field.get(object));
					break;
				case TYPE_BOOLEAN:
					json.put(columnName, field.getBoolean(object));
					break;
				case TYPE_SHORT:
					json.put(columnName, field.getShort(object));
					break;
				case TYPE_INT:
					json.put(columnName, field.getInt(object));
					break;
				case TYPE_LONG:
					json.put(columnName, field.getLong(object));
					break;
				case TYPE_FLOAT:
					json.put(columnName, field.getFloat(object));
					break;
				case TYPE_DOUBLE:
					json.put(columnName, field.getDouble(object));
					break;
				case TYPE_BLOB:
					json.put(columnName, field.get(object));
					break;
				case TYPE_JSON_OBJECT:
					if (field.get(object) != null) {
						json.put(columnName, ((JSONObject) field.get(object)).toString());
					} else {
						json.put(columnName, new JSONObject().toString());
					}
					break;
				case TYPE_JSON_ARRAY:
					if (field.get(object) != null) {
						json.put(columnName, ((JSONArray) field.get(object)).toString());
					} else {
						json.put(columnName, new JSONArray().toString());
					}
					break;
				case TYPE_FILE:
					if (field.get(object) != null) {
						json.put(columnName, ((File) field.get(object)).getAbsolutePath());
					}
					break;
				}
			}
		} catch (JSONException e) {
			Log.w(TAG, "Cannot convert cursor to json object", e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
		return json;
	}

	public String[] getProjections() {
		ArrayList<String> projections = new ArrayList<String>();
		for (Column column : mColumns) {
			projections.add(column.name);
		}
		return projections.toArray(new String[projections.size()]);
	}

	public Cursor queryAll(Context c, String selection, String[] selectionArgs) {
		return queryAll(c, selection, selectionArgs, null);
	}

	public Cursor queryAll(Context c, String selection, String[] selectionArgs, String order) {
		return c.getContentResolver().query(getUri(c), getProjections(), selection, selectionArgs, order);
	}

	// added by Yi
	public int resetAll(SQLiteDatabase db) {
		// reset upload_time to 0: data will be resend next time
		ContentValues values = new ContentValues();
		values.put(UploadEntry.Columns.UPLOAD_TIME, "0");
		return db.update(getTableName(), values, null, null);
	}

	public <T extends Entry> T cursorToObject(Cursor cursor, T object) {
		try {
			for (int columnIndex = 0; columnIndex < mColumns.length; columnIndex++) {
				Column column = mColumns[columnIndex];
				Field field = column.field;
				switch (column.type) {
				case TYPE_STRING:
					field.set(object, cursor.isNull(columnIndex) ? null : cursor.getString(columnIndex));
					break;
				case TYPE_BOOLEAN:
					field.setBoolean(object, cursor.getShort(columnIndex) == 1);
					break;
				case TYPE_SHORT:
					field.setShort(object, cursor.getShort(columnIndex));
					break;
				case TYPE_INT:
					field.setInt(object, cursor.getInt(columnIndex));
					break;
				case TYPE_LONG:
					field.setLong(object, cursor.getLong(columnIndex));
					break;
				case TYPE_FLOAT:
					field.setFloat(object, cursor.getFloat(columnIndex));
					break;
				case TYPE_DOUBLE:
					field.setDouble(object, cursor.getDouble(columnIndex));
					break;
				case TYPE_BLOB:
					field.set(object, cursor.isNull(columnIndex) ? null : cursor.getBlob(columnIndex));
					break;
				case TYPE_JSON_OBJECT:
					try {
						field.set(object, new JSONObject(cursor.getString(columnIndex)));
					} catch (IllegalArgumentException e) {
						Log.w(TAG, "unable to change cursor to value", e);
					} catch (JSONException e) {
						Log.w(TAG, "unable to change cursor to value", e);
					}
					break;
				case TYPE_JSON_ARRAY:
					try {
						field.set(object, new JSONArray(cursor.getString(columnIndex)));
					} catch (IllegalArgumentException e) {
						Log.w(TAG, "unable to change cursor to value", e);
					} catch (JSONException e) {
						Log.w(TAG, "unable to change cursor to value", e);
					}
					break;
				case TYPE_FILE:
					try {
						String filePath = cursor.getString(columnIndex);
						if (filePath != null) {
							field.set(object, new File(filePath));
						}
					} catch (IllegalArgumentException e) {
						Log.w(TAG, "unable to change cursor to value", e);
					}
					break;
				}
			}
			return object;
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public JSONObject cursorToJson(Cursor cursor) {
		JSONObject json = new JSONObject();
		try {
			for (int i = 0; i < mColumns.length; i++) {
				if (!mColumns[i].requireUpload) {
					continue;
				}
				switch (mColumns[i].type) {
				case TYPE_STRING:
					json.put(mColumns[i].name, cursor.isNull(i) ? null : cursor.getString(i));
					break;
				case TYPE_BOOLEAN:
					json.put(mColumns[i].name, cursor.getShort(i) == 1);
					break;
				case TYPE_SHORT:
					json.put(mColumns[i].name, cursor.getShort(i));
					break;
				case TYPE_INT:
					json.put(mColumns[i].name, cursor.getInt(i));
					break;
				case TYPE_LONG:
					json.put(mColumns[i].name, cursor.getLong(i));
					break;
				case TYPE_FLOAT:
					json.put(mColumns[i].name, cursor.getFloat(i));
					break;
				case TYPE_DOUBLE:
					json.put(mColumns[i].name, cursor.getDouble(i));
					break;
				case TYPE_BLOB:
					break;
				case TYPE_JSON_OBJECT:
					json.put(mColumns[i].name, new JSONObject(cursor.getString(i)));
					break;
				case TYPE_JSON_ARRAY:
					json.put(mColumns[i].name, new JSONArray(cursor.getString(i)));
				case TYPE_FILE:
					json.put(mColumns[i].name, cursor.getString(i));

				}
			}
		} catch (JSONException e) {
			Log.w(TAG, "Cannot convert cursor to json object", e);
		}
		return json;
	}

	public static final class Column {
		public final String name;
		public final int type;
		public final Field field;
		public final String defaultValue;
		public final boolean requireUpload;

		public Column(String name, int type, Field field, String defaultValue, boolean requireUpload) {
			this.name = name;
			this.type = type;
			this.field = field;
			this.defaultValue = defaultValue;
			this.requireUpload = requireUpload;
		}

		public boolean isId() {
			return Entry.Columns.Id.equals(name);
		}
	}
}
