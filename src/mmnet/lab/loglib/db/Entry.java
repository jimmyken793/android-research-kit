package mmnet.lab.loglib.db;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public abstract class Entry {

    public abstract EntrySchema getSchema();

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    public @interface Column {
        String value();

        String defaultValue() default "";

        boolean requireUpload() default true;
    }

    /* Annotation Definition */
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface Table {
        String value();
    }

    public static interface Columns {
        public static final String Id = "_id";
        public static final String UPLOAD_TIME = "upload_time";
    }

    @Column(value = "_id", requireUpload = false)
    public long id = 0;

}
