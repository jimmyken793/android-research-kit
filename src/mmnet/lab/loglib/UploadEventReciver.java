package mmnet.lab.loglib;

import mmnet.lab.C;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

public class UploadEventReciver extends BroadcastReceiver {
	private static final String LOG_TAG = "LogLib";

	@Override
	public void onReceive(Context context, Intent intent) {
		context.startService(new Intent(context, UploadService.class));
	}

	public static PendingIntent build_upload_intent(Context context) {
		Intent intent = new Intent(context, UploadEventReciver.class);
		return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	}

	public static void setAlarm(Context context) {
		AlarmManager alarms = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		PendingIntent elapsedIntent = build_upload_intent(context);
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int uploadPeriod = prefs.getInt("loglib_upload_period", C.UPLOAD_PERIOD);
		alarms.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + uploadPeriod, uploadPeriod, elapsedIntent);
		Log.d(LOG_TAG, "Alarm reset!");
	}
}
