package mmnet.lab.loglib;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.Date;
import java.util.Map;

import mmnet.lab.C;
import mmnet.lab.loglib.db.UploadEntry;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

public class UploadRunnable implements Runnable {
	private static final String TAG = "UploadThread";
	private final Map<Class<? extends UploadEntry>, String> mUrlEntryMap;
	private Context mContext;

	public UploadRunnable(Map<Class<? extends UploadEntry>, String> urlEntryMap, Context context) {
		mUrlEntryMap = urlEntryMap;
		mContext = context;
	}

	@Override
	public void run() {
		Handler handler = new Handler();
		handler.removeCallbacks(this);

		boolean error = false;
		for (Class<? extends UploadEntry> entryClass : mUrlEntryMap.keySet()) {
			try {
				Method method = entryClass.getMethod("uploadTable", Context.class, entryClass.getClass(), String.class);
				method.invoke(null, mContext, entryClass, mUrlEntryMap.get(entryClass));
				Log.i(TAG, "upload table " + entryClass.toString());
			} catch (Exception e) {
				Log.e(TAG, "error when posing data...", e);
				Log.e(TAG, mUrlEntryMap.get(entryClass));
				error = true;
			}
		}
		if (!error) {
			mContext.getSharedPreferences(C.PRE_LOGLIB, Context.MODE_PRIVATE).edit().putString(C.PRESTRING_LASTUPLOAD, DateFormat.getDateTimeInstance().format(new Date())).commit();
		}

	}
}
