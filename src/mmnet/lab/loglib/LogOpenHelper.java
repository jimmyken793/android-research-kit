package mmnet.lab.loglib;

import java.util.Set;

import mmnet.lab.loglib.db.UploadEntry;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class LogOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = "LogOpenHelper";
    private static final String DB_NAME = "log.db";

    private final Set<Class<? extends UploadEntry>> mEntrys;

    public LogOpenHelper(Context context, Set<Class<? extends UploadEntry>> entrys, int version) {
        super(context, DB_NAME, null, version);
        mEntrys = entrys;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for(Class<? extends UploadEntry> entry: mEntrys) {
            try {
                entry.newInstance().getSchema().createTable(db);
            } catch (IllegalAccessException e) {
                Log.w(TAG, "Error when create table", e);
            } catch (InstantiationException e) {
                Log.w(TAG, "Error when create table", e);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //TODO: We drop old table an create a new one now,
        // but we should move the old data too
        for(Class<? extends UploadEntry> entry: mEntrys) {
            try {
                entry.newInstance().getSchema().dropTable(db);
            } catch (IllegalAccessException e) {
                Log.w(TAG, "Error when create table", e);
            } catch (InstantiationException e) {
                Log.w(TAG, "Error when create table", e);
            }
        }
        onCreate(db);
    }
}