package mmnet.lab.loglib;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import mmnet.lab.C;
import mmnet.lab.loglib.db.EntrySchema;
import mmnet.lab.loglib.db.UploadEntry;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources.NotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;

import com.ntu.mhci.toolkit.BuildConfig;
import com.ntu.mhci.toolkit.R;

public class Util {
	private static final String TAG = "Util";
	private static final String MODE = BuildConfig.DEBUG ? "development" : "production";

	public static void assertTrue(boolean cond) {
		if (!cond) {
			throw new AssertionError();
		}
	}

	public static void triggerUpload(Context c) {
		c.sendBroadcast(new Intent(c, UploadEventReciver.class));
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity == null) {
			Log.w(TAG, "Unable to get system's Connectivity Manager");
			return false;
		} else {
			NetworkInfo[] infos = connectivity.getAllNetworkInfo();
			if (infos != null) {
				for (NetworkInfo info : infos) {
					if (info.getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static EntrySchema getSchemaFromEntryClass(Class<? extends UploadEntry> entryClass) {
		try {
			return entryClass.newInstance().getSchema();
		} catch (IllegalAccessException e) {
			Log.w(TAG, "Error when instance schema...", e);
			return null;
		} catch (InstantiationException e) {
			Log.w(TAG, "Error when instance schema...", e);
			return null;
		}
	}

	public static UploadEntry getEntryFromEntryClass(Class<? extends UploadEntry> entryClass) {
		try {
			return entryClass.newInstance();
		} catch (IllegalAccessException e) {
			Log.w(TAG, "Error when instance ...", e);
			return null;
		} catch (InstantiationException e) {
			Log.w(TAG, "Error when instance ...", e);
			return null;
		}
	}

	public static HttpResponse postJSONData(String data, String url) throws ClientProtocolException, IOException, UnsupportedEncodingException {
		HttpPost httpRequest = new HttpPost(url);
		ArrayList<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("json", data));
		httpRequest.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
		return new DefaultHttpClient().execute(httpRequest);
	}

	private static Map<Class<? extends UploadEntry>, String> sUrlEntryMap = null;
	private static Integer sDatabaseVersion = null;

	private static void initXmlConfig(Context c) {
		if (sUrlEntryMap == null || sDatabaseVersion == null) {
			sUrlEntryMap = new HashMap<Class<? extends UploadEntry>, String>();
			Context cxt = c;
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document document = builder.parse(cxt.getResources().openRawResource(R.raw.loglib));
				Element root = document.getDocumentElement();
				try {
					sDatabaseVersion = Integer.parseInt(root.getAttribute("version"));
				} catch (Exception e) {
				}
				try {
					C.UPLOAD_PERIOD = Integer.parseInt(root.getAttribute("upload_period"));
				} catch (Exception e) {
				}
				NodeList nodes = root.getElementsByTagName("Config");
				for (int i = 0; i < nodes.getLength(); i++) {
					if (Element.class.isInstance(nodes.item(i))) {
						Element ele = (Element) nodes.item(i);
						String type = ele.getAttribute("type");
						if (MODE.equals(type)) {
							processConfig(ele.getChildNodes());
						} 
					}
				}
			} catch (NotFoundException e) {
				Log.wtf("Loglib", "You DID NOT provide a /res/raw/loglib.xml in your project!");
				e.printStackTrace();
			} catch (SAXException e) {
				Log.wtf("Loglib", "Error when parsing /res/raw/loglib.xml. Grab a grammar checker!!");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ParserConfigurationException e) {
				Log.wtf("Loglib", "Error when parsing /res/raw/loglib.xml.");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				Log.wtf("Loglib", "Error when parsing /res/raw/loglib.xml. Class specified not found.");
				e.printStackTrace();
			} catch (ClassCastException e) {
				Log.wtf("Loglib", "Error when parsing /res/raw/loglib.xml. Class specified not a instance of " + UploadEntry.class.getName() + " .");
				e.printStackTrace();
			}
		}
	}

	private static void processConfig(NodeList configs) throws ClassNotFoundException {
		for (int ii = 0; ii < configs.getLength(); ii++) {
			if (Element.class.isInstance(configs.item(ii))) {
				Element configEle = (Element) configs.item(ii);
				if ("EntryType".equals(configEle.getTagName())) {
					@SuppressWarnings("unchecked")
					Class<? extends UploadEntry> clazz = (Class<? extends UploadEntry>) Class.forName(configEle.getAttribute("class_name"));
					sUrlEntryMap.put(clazz, configEle.getAttribute("upload_url"));
				}
			}
		}
	}

	public static boolean isLoggerLibEnabled(Context c) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);
		boolean loggerlib_enabled = pref.getBoolean("loggerlib_enabled", false);
		return loggerlib_enabled;
	}

	public static boolean isCelluarUploadEnabled(Context c) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);
		boolean cellular_upload = pref.getBoolean("cellular_upload", false);
		return cellular_upload;
	}

	public static void setLoggerLibEnabled(Context c, boolean enabled) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);
		pref.edit().putBoolean("loggerlib_enabled", enabled).commit();
	}

	public static void setCelluarUploadEnabled(Context c, boolean enabled) {
		SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(c);
		pref.edit().putBoolean("cellular_upload", enabled).commit();
	}

	public static Map<Class<? extends UploadEntry>, String> getUrlEntryMap(Context c) {
		initXmlConfig(c);
		return sUrlEntryMap;
	}

	public static int getDatabaseVersion(Context c) {
		initXmlConfig(c);
		return sDatabaseVersion;
	}

	public static String getLastUploadTime(Context cxt) {
		return cxt.getSharedPreferences(C.PRE_LOGLIB, 0).getString(C.PRESTRING_LASTUPLOAD, "尚未上傳");
	}
}
